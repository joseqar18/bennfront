import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignUpComponent } from './sign-up.component';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
    {
        path: '',
        component: SignUpComponent
    }
];

@NgModule({
    imports: [
        [RouterModule.forChild(routes)],
        CommonModule],

    declarations: [SignUpComponent]
})
export class SignUpModule {}
