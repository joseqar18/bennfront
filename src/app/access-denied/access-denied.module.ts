import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccessDeniedComponent } from './access-denied.component';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
    {
        path: '', component: AccessDeniedComponent
    }
];

@NgModule({
  imports: [
    [RouterModule.forChild(routes)],
    CommonModule,
  ],
  exports: [RouterModule],
  declarations: [AccessDeniedComponent]
})
export class AccessDeniedModule { }
